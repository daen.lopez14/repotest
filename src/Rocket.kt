abstract class Rocket : SpaceShip {


    abstract var rocketCost: Int
    abstract var rocketWeight: Int
    abstract var maxWeight: Int
    abstract var currentWeight: Int

    override fun launch(): Boolean {
        TODO("Not yet implemented")
    }

    override fun land(): Boolean {
        TODO("Not yet implemented")
    }

    override fun canCarry(item: Item): Boolean {
        return currentWeight + item.weight <= maxWeight
    }

    override fun carry(item: Item): Int {
        currentWeight += item.weight
        return currentWeight
    }
}