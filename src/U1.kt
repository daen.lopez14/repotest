import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random


class U1() : Rocket() {

    override var rocketCost = 100000000
    override var rocketWeight = 10000
    override var maxWeight = 18000
    override var currentWeight = rocketWeight

    override fun launch(): Boolean {
        //5% * (cargo carried/cargo limit)
        val randomValue = (Random.nextDouble(from = 0.0, until = 0.2)).toFloat()
        val chanceOfExplosion = 0.05f * (currentWeight.toFloat() / maxWeight.toFloat())
        return chanceOfExplosion <= randomValue
    }

    override fun land(): Boolean {
        //chance of landing crash = 1% * (cargo carried/cargo limit)
        val randomValue = (Random.nextDouble(from = 0.0, until = 0.2)).toFloat()
        val chanceOfCrash = 0.01f * (currentWeight.toFloat() / maxWeight.toFloat())
        return chanceOfCrash <= randomValue
    }

    fun loadItems(fileName: String): ArrayList<Item> {
        val items: ArrayList<Item> = ArrayList()
        Scanner(File(fileName)).apply {
            while (hasNextLine()) {
                val lineArray = nextLine().split(SPLIT_VALUE)
                items.add(
                    Item(lineArray[0], Integer.parseInt(lineArray[1]))
                )
            }
        }
        return items
    }

    companion object {
        private const val PHASE_1_FILE_NAME = "phase-1.txt"
        private const val PHASE_2_FILE_NAME = "phase-2.txt"
        private const val SPLIT_VALUE = "="
    }

    fun loadU1(items: ArrayList<Item>): ArrayList<Rocket> {
        val rocketsU1: ArrayList<Rocket> = ArrayList()
        var count = 0
        var rocketU1 = U1()
        for (item in items) {
            if (rocketU1.canCarry(item)) {
                rocketU1.carry(item)
                count++
                if (count == items.size) {
                    rocketsU1.add(rocketU1)
                    println("1 rocket added")
                }
            } else {
                rocketsU1.add(rocketU1)
                println("1 rocket added")
                rocketU1 = U1()
                rocketU1.carry(item)
                count++
            }
        }
        return rocketsU1
    }
}