import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class Simulation {

    fun runSimulation(rockets: ArrayList<Rocket>): Int {
        var totalBudget = 0
        var failedTrial = false
        for (rocket in rockets) {
            if (rocket.land() && rocket.launch()) {
                print("Rocket landed successfully \n")
                totalBudget += rocket.rocketCost
            } else {
                print("Rocket Failed to land \n")
                totalBudget += rocket.rocketCost
                failedTrial = true
                while(failedTrial == true){
                    if (rocket.land() && rocket.launch()) {
                        print("Rocket landed successfully \n")
                        totalBudget += rocket.rocketCost
                        failedTrial = false
                    } else {
                        print("Rocket Failed to land \n")
                        totalBudget += rocket.rocketCost
                    }
                }
            }
        }
        return totalBudget
    }

    fun loadItems(fileName: String): ArrayList<Item> {
        val items: ArrayList<Item> = ArrayList()
        Scanner(File(fileName)).apply {
            while (hasNextLine()) {
                val lineArray = nextLine().split(SPLIT_VALUE)
                items.add(
                    Item(lineArray[0], Integer.parseInt(lineArray[1]))
                )
            }
        }
        return items
    }

    companion object {
        private const val PHASE_1_FILE_NAME = "phase-1.txt"
        private const val PHASE_2_FILE_NAME = "phase-2.txt"
        private const val SPLIT_VALUE = "="
    }

    fun loadU1(items: ArrayList<Item>): ArrayList<Rocket> {
        val rocketsU1: ArrayList<Rocket> = ArrayList()
        //var count = 0
        var rocketU1 = U1()
        for (item in items) {
            if (rocketU1.canCarry(item)) {
                rocketU1.carry(item)
            } else {
                rocketsU1.add(rocketU1)
                println("1 rocket added")
                rocketU1 = U1()
                rocketU1.carry(item)
            }
        }
        return rocketsU1
    }
}

fun runSimulation(rockets: ArrayList<Rocket>): Int {
    var totalBudget = 0
    var failedTrials = 0
    for (rocket in rockets) {
        if (rocket.land() && rocket.launch()) {
            print("Rocket landed successfully \n")
            totalBudget += rocket.rocketCost
        } else {
            print("Rocket Failed to land \n")
            totalBudget += rocket.rocketCost
            failedTrials++
            while (failedTrials != 0) {
                if (rocket.land() && rocket.launch()) {
                    print("Rocket landed successfully \n")
                    totalBudget += rocket.rocketCost
                    failedTrials--
                } else {
                    print("Rocket Failed to land \n")
                    totalBudget += rocket.rocketCost
                    failedTrials++
                }
            }
        }
    }
    return totalBudget
}