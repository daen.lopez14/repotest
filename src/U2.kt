import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random

class U2() : Rocket() {

    override var rocketCost = 120000000
    override var rocketWeight = 18000
    override var maxWeight = 29000
    override var currentWeight = rocketWeight

    override fun launch(): Boolean {
        //4% * (cargo carried/cargo limit)
        val randomValue = (Random.nextDouble(from = 0.0, until = 1.0)).toInt()
        val chanceOfExplosion = 4.0 * (currentWeight / maxWeight)
        return chanceOfExplosion <= randomValue
    }

    override fun land(): Boolean {
        //chance of landing crash = 8% * (cargo carried/cargo limit)
        val randomValue = (Random.nextDouble(from = 0.0, until = 1.0)).toInt()
        val chanceOfCrash = 8.0 * (currentWeight / maxWeight)
        return chanceOfCrash <= randomValue
    }

    fun loadItems(fileName: String): ArrayList<Item> {
        val items: ArrayList<Item> = ArrayList()
        Scanner(File(fileName)).apply {
            while (hasNextLine()) {
                val lineArray = nextLine().split(SPLIT_VALUE)
                items.add(
                    Item(lineArray[0], Integer.parseInt(lineArray[1]))
                )
            }
        }
        return items
    }

    companion object {
        private const val PHASE_1_FILE_NAME = "phase-1.txt"
        private const val PHASE_2_FILE_NAME = "phase-2.txt"
        private const val SPLIT_VALUE = "="
    }

    fun loadU2(items: ArrayList<Item>): ArrayList<Rocket> {
        val rocketsU2: ArrayList<Rocket> = ArrayList()
        var count = 0
        var rocketU2 = U2()
        for (item in items) {
            if (rocketU2.canCarry(item)) {
                rocketU2.carry(item)
                count++
                if (count == items.size) {
                    rocketsU2.add(rocketU2)
                    println("1 rocket added-")
                }
            } else {
                rocketsU2.add(rocketU2)
                println("1 rocket added")
                rocketU2 = U2()
                rocketU2.carry(item)
                count++
                if (count == items.size) {
                    rocketsU2.add(rocketU2)
                    println("1 rocket added-")
                }
            }
        }
        return rocketsU2
    }
}